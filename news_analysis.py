from text_predictor import TextPredictor
import time

def main(train=False):
    
    start_time = time.time()

    model_filepath = r'models\test'
    categories = ['REAL', 'FAKE']
    predictor = TextPredictor(categories, use_gpu=False, text_label='title')
    predictor.load_data('classifier_data/fake_or_real_news.csv', cat_column_header='label')

    if train:
        predictor.create_model()
        predictor.train(n_epochs=2)
    else:
        predictor.from_disc(model_filepath)

    predictor.validate()

    predictor.to_disc(model_filepath)

    end_time = time.time()
    execution_time = end_time - start_time
    print("Exectution time: %.4f" % execution_time)

if __name__ == '__main__':
    main(train=False)