from textblob.classifiers import DecisionTreeClassifier, NaiveBayesClassifier
import csv
import spacy

class Predictor():

    def __init__(self):
        self.train_set_filepath = 'classifier_data/train_set.csv'
        self.test_set_filepath = 'classifier_data/test_set.csv'

    def train(self, classifier_type = 'NaiveBayes'):
        """ Trains a Classifier on the training data.

            Args:
                classifier_type -- NaiveBayes or DecisionTree (default = NaiveBayes)

            Returns:
                Classifier accuracy """
        with open(self.train_set_filepath, 'r') as f:
            if classifier_type == 'NaiveBayes':
                self.classifier = NaiveBayesClassifier(f, format='csv')
            elif classifier_type == "DecisionTree":
                self.classifier = DecisionTreeClassifier(f, format='csv')
            else:
                raise Exception("%s classifier does not exist." % classifier_type)

        with open(self.test_set_filepath, 'r') as f:
            accuracy = self.classifier.accuracy(f, format='csv')
        return accuracy

    # def train(self, classifier_type = 'NaiveBayes'):
    #     """ Trains a Classifier on the training data.

    #     Args:
    #         classifier_type -- NaiveBayes or DecisionTree (default = NaiveBayes)

    #     Returns:
    #         Classifier accuracy """
    # with open(self.train_set_filepath, 'r') as f:
    #     if classifier_type == 'NaiveBayes':
    #         self.classifier = NaiveBayesClassifier(f, format='csv')
    #     elif classifier_type == "DecisionTree":
    #         self.classifier = DecisionTreeClassifier(f, format='csv')
    #     else:
    #         raise Exception("%s classifier does not exist." % classifier_type)

    # with open(self.test_set_filepath, 'r') as f:
    #     accuracy = self.classifier.accuracy(f, format='csv')
    # return accuracy


    def classify(self, title):
        return self.classifier(title)