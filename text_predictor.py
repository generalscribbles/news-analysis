import pandas as pd
import spacy
from spacy.util import minibatch
from spacy.training import Example
import random
import cupy as cp
import numpy as np

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from spacy.pipeline.textcat import DEFAULT_SINGLE_TEXTCAT_MODEL

class TextPredictor():
    """ Initializes an NLP model based on the spaCy library.
        Can be used to classify between N exclusive categories based on text.

        Args:
            
            categories      -- list of N category labels (str)
        Optional:
            text_label      -- the label in the csv to be used as the text. default == 'text'"""

    def __init__(self, categories, text_label='text', use_gpu=False):
        self.cat_labels = [cat for cat in categories]
        self.text_label = text_label
        
        # Create an empty model
        if use_gpu:
            spacy.require_gpu()
        self.use_gpu = use_gpu            

        
    def create_model(self):
        ''' Creates a textcat model to be used in training '''
        self.nlp = spacy.blank('en')

        # Create a TextCategorizer
        self.nlp.add_pipe(
                        'textcat',
                        config={
                            'threshold': 0.5,
                            'model': DEFAULT_SINGLE_TEXTCAT_MODEL})

        # Add labels for the text classifier
        for cat in self.cat_labels:
            self.nlp.get_pipe('textcat').add_label(cat)

    def load_data(self, csv_filepath, encoding='utf-8', dataset_columns=None, dataset_columns_to_keep='', cat_column_header='label'):
        """ Loads the data for both categories and splits it into training and validation data.  """
        print("Loading data...")
        data = pd.read_csv(csv_filepath, encoding=encoding, names=dataset_columns)

        if dataset_columns_to_keep:
            data = data[dataset_columns_to_keep]
        
        # Convert label column to string for the spaCy textcat
        if data.dtypes[cat_column_header] is not str:
            data[cat_column_header] = data[cat_column_header].astype(str)

        # Split data into training and validation data
        self.train_texts, self.val_texts, self.train_labels, self.val_labels = train_test_split(data[self.text_label],
                                                                                            data[cat_column_header],
                                                                                            random_state=0)

        # Format the training labels for the model
        train_labels = [{'cats': {cat: label == cat}
                                for cat in self.cat_labels}
                                for label in self.train_labels]
        
        self.train_data = list(zip(self.train_texts, train_labels))
        print('Data successfully loaded.')

    def train(self, n_epochs=10):
        """ Trains the model

            Args:
                n_epochs    -- the number of epochs to run """

        print('Beginning training...')
        spacy.util.fix_random_seed(1)
        optimizer = self.nlp.initialize()

        losses = {}
        for epoch in range(n_epochs):
            random.shuffle(self.train_data)
            # Create the batch generator
            batches = minibatch(self.train_data, size=50)

            # Iterate
            for batch in batches:
                texts, labels = zip(*batch)

                # Create list of Examples for spacy update
                docs = [self.nlp.tokenizer(text) for text in texts]                
                examples = [Example.from_dict(doc, label) for doc, label in zip(docs, labels)]

                self.nlp.update(examples, sgd=optimizer, losses=losses)
                
            print("Epoch (%d/%d). Losses: %.4f" % (epoch, n_epochs, losses['textcat']))
        
        print('Training complete.')
        
    def validate(self):
        """ Validates the trained model by predicting on the validation data
            
            Returns:
                Accuracy of the predicted data vs the validation data """
        print('Beginning validation...')
        docs = [self.nlp.tokenizer(text) for text in self.val_texts]

        textcat = self.nlp.get_pipe('textcat')

        # Iterate over docs.
        # Needs to go one at a time because if the test dataset is too big it crashes.
        scores = []
        predicted = []
        for doc in docs:
            result = textcat.predict([doc])
            scores.append(result[0])

        # Get the prediction.
        # Need to use cp.asnumpy because if using GPU, it does not return a numpy array       
        if self.use_gpu:
            scores = cp.asarray(scores)
            scores = cp.asnumpy(scores)
        else:
            scores = np.asarray(scores)

        predicted = scores.argmax(axis=1)
        predicted_labels = [textcat.labels[label] for label in predicted]        

        # Calculate accuracy
        accuracy = self.calculate_accuracy(list(self.val_labels), predicted_labels)

        print('Validation complete.')
        return accuracy
        

    def calculate_accuracy(self, validation, predicted):
        """ Calculates accuracy between two lists of labels
            
            Args:
                validation    -- list of validation labels
                predicted     -- list of predicted labels
                
            Returns:
                Accuracy value"""
        avg_acc = accuracy_score(validation, predicted)

        print("\nAccuracy: %.4f" % avg_acc)
        return avg_acc

    def to_disc(self, out_filepath):
        self.nlp.to_disk(out_filepath)

    def from_disc(self, in_filepath):
        self.nlp = spacy.load(in_filepath)