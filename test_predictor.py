import unittest
from text_predictor import TextPredictor


class TestPredictor(unittest.TestCase):

    def setUp(self):
        categories = ['REAL', 'FAKE']
        self.predictor = TextPredictor(categories, text_label='title')
        self.predictor.load_data(r'../../unittest/text_predictor/fake_or_real_news.csv', cat_column_header='label')

    def test_model_new(self):
        self.predictor.create_model()
        self.predictor.train(n_epochs=2)
        actual_accuracy = self.predictor.validate()

        expected_accuracy = 0.8384
        self.assertAlmostEqual(expected_accuracy, actual_accuracy, 3)

    def test_model_from_disk(self):
        self.predictor.from_disc(r'..\..\unittest\text_predictor\unittest_model')
        actual_accuracy = self.predictor.validate()

        expected_accuracy = 0.8384
        self.assertAlmostEqual(expected_accuracy, actual_accuracy, 3)

if __name__ == '__main__':
    unittest.main()

   
